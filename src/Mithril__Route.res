open Belt
open Webapi

type t
@module("mithril") external _route: t = "route"
@module("mithril") external mount: (Dom.Element.t, string, Js.Dict.t<'a>) => unit = "route"

// prefix
@set external _setPrefix: (t, string) => unit = "prefix"
@inline let setPrefix = prefix => _route->_setPrefix(prefix)

// set

type options<'state> = {
  replace?: bool,
  title?: string,
  state?: 'state,
}

@module("mithril") @scope("route")
external set: (~url: string, ~params: 'params=?, ~options: options<'state>=?, unit) => unit = "set"

// get
@module("mithril") @scope("route") external get: unit => option<string> = "get"
@module("mithril") @scope("route") external getUnsafe: unit => string = "get"

@module("mithril") @scope("route")
external param: string => option<string> = "param"
let paramInt = (name: string): option<int> => name->param->Option.flatMap(Int.fromString)

%%private(@val external isTruish: 'a => bool = "Boolean")
let paramBool = (name: string): option<bool> => name->param->Option.map(isTruish)

@deprecated("Use parameters or param instead.  This is not type-safe.")
@module("mithril")
@scope("route")
external allParams: unit => 'a = "param"

@doc("Get many parameters at once.  Returns an array of optional strings")
let parameters = names => names->Belt.Array.map(param)

@scope("window.history") @val external back: unit => unit = "back"
@scope("window.history") @val external go: int => unit = "go"

// title
let title = () =>
  Dom.document
  ->Dom.Document.asHtmlDocument
  ->Option.map(doc => doc->Dom.HtmlDocument.title)
  ->Option.getWithDefault("")

let setTitle = newTitle =>
  Dom.document
  ->Dom.Document.asHtmlDocument
  ->Option.map(doc => doc->Dom.HtmlDocument.setTitle(newTitle))
  ->ignore
