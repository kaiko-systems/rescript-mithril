// Use for debugging purposes
// %%raw(`
// import mithril from 'mithril';
// window.mithril = mithril;
// `)

open Belt
open Webapi

// bind to JS Array.prototype.join, otherwise we would have to do something like:
//
//   ->Belt.Array.reduce(" ", (a, b) => a ++ b)
//
@send external join: (array<'t>, string) => string = "join"

module Style = ReactDOMStyle
module Request = Mithril__Request
module Route = Mithril__Route

type element = React.element

@module("mithril") external mount: (Webapi.Dom.Element.t, 'a) => unit = "mount"
@module("mithril") external redraw: unit => unit = "redraw"
@module("mithril") @scope("redraw") external redrawSync: unit => unit = "sync"
@module("mithril") external trust: string => element = "trust"
@module("mithril") external tag: (string, 'props, array<element>) => element = "default"
@module("mithril")
external componentTag: (React.component<'props>, 'props, array<element>) => element = "default"
external data: Js.t<'a> => string = "%identity"
// State handling

type rec component<'state, 'attrs> = {
  @as("data") state: 'state,
  oninit?: vnode<'state, 'attrs> => unit,
  oncreate?: vnode<'state, 'attrs> => unit,
  view?: vnode<'state, 'attrs> => element,
  onbeforeupdate?: (vnode<'state, 'attrs>, vnode<'state, 'attrs>) => bool,
  onupdate?: vnode<'state, 'attrs> => unit,
  onbeforeremove?: vnode<'state, 'attrs> => Js.Promise.t<unit>,
  onremove?: vnode<'state, 'attrs> => unit,
}
and state<'state> = {mutable data: 'state}
and vnode<'state, 'attrs> = {
  @as("state") component: state<'state>,
  attrs: 'attrs,
  dom: option<Webapi.Dom.Node.t>,
  domSize: option<int>,
  children: array<element>,
}

@get @scope("state") external state: vnode<'state, 'attrs> => 'state = "data"
@set @scope("state")
external setStateWithoutRedraw: (vnode<'state, 'attrs>, 'state) => unit = "data"

@inline
let setState = (vnode, state) => {
  vnode->setStateWithoutRedraw(state)
  redraw()
}

// HTMLElement helpers

module Element = {
  let select = (vnode, selector) => {
    switch vnode.dom {
    | Some(dom) =>
      if vnode.domSize->Option.getWithDefault(1) == 1 {
        dom->Dom.Element.ofNode
      } else {
        dom->Dom.Node.parentElement
      }->Option.flatMap(Dom.Element.querySelector(_, selector))
    | None => None
    }
  }

  external toElement: 'a => option<Dom.Element.t> = "%identity"

  let asHtmlElement = element => element->Option.flatMap(Dom.Element.asHtmlElement)
  external _asHtmlInputElement: Dom.Element.t => Dom.HtmlInputElement.t = "%identity"
  let asHtmlInputElement = element => element->Option.map(_asHtmlInputElement)

  let value = element => {
    element->asHtmlElement->Option.flatMap(element => Some(element->Dom.HtmlElement.value))
  }

  let setValue = (element, value) => {
    element
    ->asHtmlInputElement
    ->Option.map(element => element->Dom.HtmlInputElement.setValue(value))
    ->ignore
  }

  @get external _files: Dom.HtmlElement.t => array<File.t> = "files"
  external fileToBlob: File.t => Blob.t = "%identity"

  let files = element => {
    element->asHtmlElement->Option.flatMap(element => Some(element->_files))
  }

  let focus = element => {
    element->asHtmlElement->Option.map(Dom.HtmlElement.focus)->ignore
  }

  let click = element => {
    element->asHtmlElement->Option.map(Dom.HtmlElement.click)->ignore
  }
}

// Component definition

external toJsxElement: 'a => Jsx.element = "%identity"

let component = (state: 'state) => {state: state}

let oninit = (component, oninit) => {...component, oninit}
let oncreate = (component, oncreate) => {...component, oncreate}
let view = (component, view) => {...component, view}->toJsxElement
let onbeforeupdate = (component, onbeforeupdate) => {
  ...component,
  onbeforeupdate,
}
let onupdate = (component, onupdate) => {...component, onupdate}
let onbeforeremove = (component, onbeforeremove) => {
  ...component,
  onbeforeremove,
}
let onremove = (component, onremove) => {...component, onremove}

// Rendering helpers

let empty = React.null
external show: 'a => element = "%identity"

let class = classes => {
  classes->Belt.Array.keepMap(((class, expression)) => expression ? Some(class) : None)->join(" ")
}

module Link = {
  type props_<'state> = {
    href?: string,
    options?: Route.options<'state>,
    className?: string,
    disabled?: bool,
    children?: Jsx.element,
    target?: string,
    style?: Style.t,
    onClick?: JsxEvent.Mouse.t => unit,
  }

  @module("mithril") @scope("route") external _link: element = "Link"
  @react.component(: props_<'state>)
  let make = () => _link
}
