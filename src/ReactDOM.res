// automatic mode

external someElement: Jsx.element => option<Jsx.element> = "%identity"

@module("mithril") external mithril: ('tag, 'props, Jsx.element) => Jsx.element = "default"

let processProps: 'props => ('props, Jsx.element) = %raw(`function(props) {
  let children = undefined;
  if (props !== undefined) {
    if (props.hasOwnProperty("children")) {
      children = props.children;
      delete props.children;
    }
    if (props.hasOwnProperty("data")) {
        Object.entries(props.data).forEach((keyValue) => {
          props["data-" + keyValue[0]] = keyValue[1];
        });
        delete props.data;
    }
    Object.entries(props).forEach((keyValue) => {
      let key = keyValue[0];
      let value = keyValue[1];
      if (key.startsWith("on")) {
        props[key.toLowerCase()] = value;
        delete props[key];
      }
    });
  }
  return [props, children];
}`)

let jsx = (tag: string, props: JsxDOM.domProps): Jsx.element => {
  let (props, children) = props->processProps
  mithril(tag, props, children)
}

let jsxs = jsx

let jsxKeyed = (tag, props, ~key: string, ()) => {
  jsx(tag, {...props, key})
}

let jsxsKeyed = (tag, props, ~key: string, ()) => {
  jsxs(tag, {...props, key})
}
