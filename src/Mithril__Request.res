type body
type method = [#GET | #POST | #PUT | #PATCH | #DELETE | #HEAD | #OPTIONS]
// type xhr

external _anyToBody: 'a => body = "%identity"
let _empty = _anyToBody(None)

type t = {
  url: string,
  method: method,
  params: Js.Dict.t<string>,
  body: body,
  headers: Js.Dict.t<string>,
  background: bool,
}

let get = url => {
  url,
  method: #GET,
  params: Js.Dict.empty(),
  body: _empty,
  headers: Js.Dict.empty(),
  background: true,
}

let post = url => {
  url,
  method: #POST,
  params: Js.Dict.empty(),
  body: _empty,
  headers: Js.Dict.empty(),
  background: true,
}

let options = url => {
  url,
  method: #OPTIONS,
  params: Js.Dict.empty(),
  body: _empty,
  headers: Js.Dict.empty(),
  background: true,
}

let put = url => {
  url,
  method: #PUT,
  params: Js.Dict.empty(),
  body: _empty,
  headers: Js.Dict.empty(),
  background: true,
}

let delete = url => {
  url,
  method: #DELETE,
  params: Js.Dict.empty(),
  body: _empty,
  headers: Js.Dict.empty(),
  background: true,
}

let patch = url => {
  url,
  method: #PATCH,
  params: Js.Dict.empty(),
  body: _empty,
  headers: Js.Dict.empty(),
  background: true,
}

let head = url => {
  url,
  method: #HEAD,
  params: Js.Dict.empty(),
  body: _empty,
  headers: Js.Dict.empty(),
  background: true,
}

external _payloadToDict: 'a => Js.Dict.t<string> = "%identity"
let payload = (request, payload) => {
  switch request.method {
  | #GET | #HEAD => {...request, params: payload->_payloadToDict}
  | _ => {...request, body: payload->_anyToBody}
  }
}

external _webFormToJsDict: Webapi.FormData.t => Js.Dict.t<string> = "%identity"
let form = (request, form) => {
  switch request.method {
  | #GET => {...request, params: form->_webFormToJsDict}
  | _ => {...request, body: form->_anyToBody}
  }
}

external _JsonToJsDict: Js.Json.t => body = "%identity"
let json = (request, body) => {
  {...request, body: body->_JsonToJsDict}
}

// Create a new request with a new set of headers.
//
// The headers are given as an array of pairs `(header, value)`.  If the
// same header appears more than once, the last value survices.
let headers = (request: t, headers: Js.Array.t<(string, string)>) => {
  {...request, headers: headers->Js.Dict.fromArray}
}

// Create a new request with extra headers.
//
// Unlike `headers`, we keep the current headers and override
// them with `extraHeaders`.
let addHeaders = (request, extraHeaders) => {
  let entries = request.headers->Js.Dict.entries->Js.Array.concat(extraHeaders)
  request->headers(entries)
}

/// FIXME: Better typing. We should opaque 'type response' to force users to
/// deal the actual type of the response (e.g application/json,
/// application/pdf, etc...)
@module("mithril") external _do: t => Js.Promise.t<'a> = "request"

let do: t => promise<result<'a, _>> = request =>
  request
  ->_do
  // response to result
  ->Js.Promise.then_(data => Js.Promise.resolve(Ok(data)), _)
  ->Js.Promise.catch(error => Js.Promise.resolve(Error(error)), _)
